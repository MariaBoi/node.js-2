const User = require("../models/User");
const bcrypt = require("bcrypt");

const jwt = require("jsonwebtoken");
SECRET_FOR_ENCR = process.env.SECRET_FOR_ENCR;
const authMiddleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];

    if (!token) {
      return res.status(400).json({ message: "no token was provided" });
    }

    const loginedUser = jwt.verify(token, SECRET_FOR_ENCR);
    console.log("log", loginedUser);
    const userExists = await User.exists({ _id: loginedUser._id });

    if (!userExists) {
      return res
        .status(400)
        .json({ message: "user not found, it may be daleted" });
    }
    req.user = {
      username: loginedUser.username,
      id: loginedUser._id,
    };
    next();
  } catch (e) {
    console.log(e);
    return res.status(400).json({ message: e.message || "not authorized eor" });
  }
};

module.exports = authMiddleware;
