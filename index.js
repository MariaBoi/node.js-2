require("dotenv").config();

const express = require("express");
const fs = require("fs");
const path = require("path");
const mongoose = require("mongoose");
const morgan = require("morgan");
const PORT = process.env.PORT;
const app = express();
const authRouter = require("./routes/authRouter");
const usersRouter = require("./routes/usersRouter");
const notesRouter = require("./routes/notesRouter");
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true }, () => {
  console.log("connected to mongo ");
});

const pathToFile = path.join(__dirname, "logger.txt");
if (!fs.existsSync(pathToFile)) {
  fs.appendFileSync("logger.txt", "", "utf-8");
}
const myStream = fs.createWriteStream(pathToFile, { flags: "a" });
app.use(morgan("combined", { stream: myStream }));

app.use(express.json());
app.use("/api/auth", authRouter);
app.use("/api/users", usersRouter);
app.use("/api/notes", notesRouter);
app.use(function (err, req, res, next) {
  console.error(err.stack);
  if (err.status && err.message) {
    res.status(err.status).json({
      message: err.message,
    });
    return;
  }
  res.status(500).json({
    message: "Internal server error",
  });
});

app.listen(PORT || 8080, () => console.log("listenind///" + PORT));
