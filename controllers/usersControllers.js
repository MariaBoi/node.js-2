const User = require("../models/User");
const bcrypt = require("bcrypt");

const jwt = require("jsonwebtoken");

const getmyProfileInfo = async (req, res) => {
  let currUser;
  try {
    currUser = await User.findOne({ username: req.user.username });
  } catch (e) {
    console.log(e);
    return res.status(500).json({ message: "not autorized  server error" });
  }

  return res.status(200).json({
    user: {
      username: currUser.username,
      createdDate: currUser.createdDate,
      _id: currUser._id,
    },
  });
};

const deleteProfile = async (req, res) => {
  try {
    result = await User.deleteOne({ username: req.user.username });
    if (result.deletedCount === 0) {
      const er = new Error("User not found");
      er.status = 400;
      throw er;
    }
    console.log(result);
  } catch (e) {
    console.log(e);
    return res.status(500).json({ message: "not authorized  server error" });
  }

  return res.status(200).json({
    message: "success",
  });
};

const changePass = async (req, res) => {
  if (!req.body.oldPassword || !req.body.newPassword) {
    return res.status(400).json({ message: "old pass or new is not here" });
  }
  try {
    const curUser = await User.findById(req.user.id);

    validOldPass = await bcrypt.compare(req.body.oldPassword, curUser.password);

    if (!validOldPass) {
      return res.status(400).json({ message: "old pass is not valid" });
    }
    const newPassHashed = await bcrypt.hash(req.body.newPassword, 12);
    await curUser.updateOne({ $set: { password: newPassHashed } });
  } catch (e) {
    console.log("error::", e);
    return res.status(500).json({ message: "server error changing pass" });
  }

  return res.status(200).json({ message: "success" });
};
module.exports = { getmyProfileInfo, deleteProfile, changePass };
