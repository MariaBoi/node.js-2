const User = require("../models/User");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Note = require("../models/Note");
const getNoteFunc = require("../utils/getNoteById");
SECRET_FOR_ENCR = process.env.SECRET_FOR_ENCR;

const createNote = async (req, res) => {
  if (!req.body.text) {
    return res.status(400).json({
      message: `no text was passed`,
    });
  }

  try {
    const newNote = new Note({
      text: req.body.text,
      userId: req.user.id,
    });
    await newNote.save();
  } catch (e) {
    console.log(e);
    return res
      .status(500)
      .json({ message: e.message || "server error, failed to create note" });
  }
  return res.status(200).json({
    message: `user: ${req.user.username} created "${req.body.text}" note`,
  });
};

const getUserNotes = async (req, res) => {
  let notes;
  try {
    count = await Note.find({ userId: req.user.id }, "-__v");
    notes = await Note.find({ userId: req.user.id }, "-__v")
      .skip(+req.query.offset)
      .limit(+req.query.limit);
  } catch (e) {
    console.log(e);
    return res
      .status(500)
      .json({ message: e.message + " server error, failed get users notes" });
  }

  return res.status(200).json({
    limit: req.query.limit,
    offset: req.query.offset,
    count: count.length,
    message: `user: ${req.user.username} has  notes`,
    notes,
  });
};

const getNotebyId = async (req, res) => {
  let note;
  try {
    note = await getNoteFunc(req.params.id, req.user.id);
  } catch (error) {
    if (error.status && error.message) {
      res.status(error.status).json({
        message: error.message,
      });
      return;
    }
  }

  if (!note) {
    return res
      .status(400)
      .json({ message: "you are not owner of note this this id" });
  }

  return res.status(200).json({
    note,
  });
};

const updateNotebyId = async (req, res) => {
  let note;
  if (!req.body.text) {
    return res.status(400).json({ message: "no text was given" });
  }

  try {
    note = await getNoteFunc(req.params.id, req.user.id);

    if (!note) {
      return res
        .status(400)
        .json({ message: "you can update only your notes" });
    }
    note.text = req.body.text;
    await note.save();
  } catch (error) {
    if (error.status && error.message) {
      res.status(error.status).json({
        message: error.message,
      });
      return;
    }
    return res
      .status(500)
      .json({ message: e.message || "server error, failed update note" });
  }

  return res.status(200).json({
    message: "sucess updated",
  });
};

const toggleNotebyId = async (req, res) => {
  let note;

  try {
    try {
      note = await getNoteFunc(req.params.id, req.user.id);
    } catch (error) {
      if (error.status && error.message) {
        res.status(error.status).json({
          message: error.message,
        });
        return;
      }
    }
    if (!note) {
      return res.status(400).json({ message: "you can check only your notes" });
    }
    note.completed = !note.completed;
    await note.save();
  } catch (e) {
    console.log(e);
    return res
      .status(500)
      .json({ message: e.message || "server error, failed to check note" });
  }

  return res.status(200).json({
    message: "success checked",
  });
};

const deleteNotebyId = async (req, res) => {
  let note;

  try {
    try {
      note = await getNoteFunc(req.params.id, req.user.id);
    } catch (error) {
      if (error.status && error.message) {
        res.status(error.status).json({
          message: error.message,
        });
        return;
      }
    }
    if (!note) {
      return res
        .status(400)
        .json({ message: "you can delete only your notes" });
    }
    note.completed = !note.completed;
    await note.remove();
  } catch (e) {
    console.log(e);
    return res
      .status(500)
      .json({ message: e.message || "server error, failed to delete note" });
  }

  return res.status(200).json({
    message: "success deleted",
  });
};
module.exports = {
  createNote,
  getUserNotes,
  getNotebyId,
  updateNotebyId,
  toggleNotebyId,
  deleteNotebyId,
};
