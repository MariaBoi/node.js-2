const {
  getmyProfileInfo,
  deleteProfile,
  changePass,
} = require("../controllers/usersControllers");
const authMiddleware = require("../middleware/authMiddleware");

const usersRouter = require("express").Router();

usersRouter.get("/me", authMiddleware, getmyProfileInfo);
usersRouter.delete("/me", authMiddleware, deleteProfile);
usersRouter.patch("/me", authMiddleware, changePass);

module.exports = usersRouter;
