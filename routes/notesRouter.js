const {
  createNote,
  getUserNotes,
  getNotebyId,
  updateNotebyId,
  toggleNotebyId,
  deleteNotebyId,
} = require("../controllers/notesController");
const authMiddleware = require("../middleware/authMiddleware");

const notesRouter = require("express").Router();

notesRouter.post("/", authMiddleware, createNote);
notesRouter.get("/", authMiddleware, getUserNotes);
notesRouter.get("/:id", authMiddleware, getNotebyId);
notesRouter.put("/:id", authMiddleware, updateNotebyId);
notesRouter.patch("/:id", authMiddleware, toggleNotebyId);
notesRouter.delete("/:id", authMiddleware, deleteNotebyId);

module.exports = notesRouter;
