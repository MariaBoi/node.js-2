const Note = require("../models/Note");

const getNoteFunc = async (id, userId) => {
  try {
    return await Note.findOne({ _id: id, userId }, "-__v");
  } catch (error) {
    console.log(error);
    const er = new Error("not wiyh this id doesnot exist");
    er.status = 400;
    throw er;
  }
};

module.exports = getNoteFunc;
