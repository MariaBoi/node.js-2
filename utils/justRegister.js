const User = require("../models/User");
const bcrypt = require("bcrypt");
const justregister = async (username, password) => {
  const hashedPass = await bcrypt.hash(password, 10);

  const userToSend = new User({
    username,
    password: hashedPass,
  });

  await userToSend.save();
};

module.exports = justregister;
