const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const noteSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, required: true },
  text: { type: String, required: true },
  completed: { type: Boolean, default: false },
  createdDate: { type: Date, default: Date.now() },
});

module.exports = mongoose.model("Note", noteSchema);
